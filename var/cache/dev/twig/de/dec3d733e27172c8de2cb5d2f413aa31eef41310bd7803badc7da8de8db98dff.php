<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* maleteo.html.twig */
class __TwigTemplate_0354121516b4e5af658f87be18534368584d5fcd745485ab3ee01c2bb8a249a6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <link rel=\"stylesheet\" href=\"styles/styles.css\">
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran&display=swap\" rel=\"stylesheet\">
    <title>Maleteo</title>
</head>
";
        // line 12
        $this->displayBlock('body', $context, $blocks);
        // line 241
        echo "
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 12
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "<body>
        <div class=\"logo\">
            <img src=\"assets/logo.svg\" alt=\"Logo Maleteo\">
        </div>
        <header class=\"compra\">
            <div class=\"wrapper\">
                <div class=\"container-fluid\">
                <section class=\"row\">
                    <div class=\"item col-xs-12 col-md-6\">
                        <h1 class=\"item__heading\">
                            Gana dinero guardando equipaje a viajeros como tu
                        </h1>
                        <ul class=\"app\">
                            <li class=\"app_tienda\">
                                <img src=\"assets/google-play.svg\" alt=\"Icono Google play\">
                            </li>
                            <li class=\"app_tienda\">
                                <img src=\"assets/app-store.svg\" alt=\"Icono\">
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
            </div>
        </header>
        <div class=\"wrapper\">
            <div class=\"how-works\">
                <h2 class=\"how-works__title heading-2\">
                    ¿Cómo funciona?
                </h2>
                <ol class=\"row\">
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                1
                            </p>
                        </div>
                        <div class=\"info\">
                            <p class=\"heading-3\">
                                Date de alta
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                2
                            </p>
                        </div>
                        <div>
                            <p class=\"heading-3\">
                                Publica tus espacios, horarios y tarifas
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                3
                            </p>
                        </div>
                        <div>
                            <p class=\"heading-3\">
                                Recibe viajeros y gana dinero
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                </ol>
            </div>
        </div>
        <div class=\"wrapper\">
            <div class=\"row\">
                <div class=\"movil col-xs-12 col-md-6\">
                    <img src=\"assets/iPhoneX.png\" alt=\"Foto iphone aplicación\" class=\"movil__item\">
                </div>
                <div class=\"col-xs-12 col-md-5\">
                    <div class=\"infocliente\">
                        <p class=\"heading-3\">
                            Solicita una demo
                        </p>
                        ";
        // line 105
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 105, $this->source); })()), 'form_start');
        echo "
                            <div class=\"infocliente__item\">
                                <!--label for=\"nombre\">
                                    <p>
                                        Nombre
                                    </p>
                                </!--label>
                                <input type=\"text\" name=\"nombre\" placeholder=\" Vincent Chase\"-->
                        ";
        // line 113
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 113, $this->source); })()), "nombre", [], "any", false, false, false, 113), 'label');
        echo "
                        ";
        // line 114
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 114, $this->source); })()), "nombre", [], "any", false, false, false, 114), 'widget');
        echo "
                            </div>
                            <div class=\"infocliente__item\">
                                <!--label for=\"mail\">
                                    <p>
                                        Email
                                    </p>
                                </--label>
                                <input type=\"email\" name=\"mail\" placeholder=\"vincent@mga.com\"-->
                        ";
        // line 123
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 123, $this->source); })()), "mail", [], "any", false, false, false, 123), 'label');
        echo "
                        ";
        // line 124
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 124, $this->source); })()), "mail", [], "any", false, false, false, 124), 'widget');
        echo "
                            </div>
                            <div class=\"infocliente__item\">
                                <!--label for=\"horario\">
                                    <p>
                                        Horario preferido
                                    </p>
                                </!--label>
                                <input type=\"text\" name=\"horario\" placeholder=\"11:00h-13:00h\"-->
                        ";
        // line 133
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 133, $this->source); })()), "horario", [], "any", false, false, false, 133), 'label');
        echo "
                        ";
        // line 134
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 134, $this->source); })()), "horario", [], "any", false, false, false, 134), 'widget');
        echo "
                            </div>
                            <div class=\"infocliente__item\">
                                <!--label for=\"cuidad\">
                                    <p>
                                        Ciudad
                                    </p>
                                </!--label>
                                <select name=\"ciudades\">
                                    <option>
                                        Madrid
                                    </option>
                                    <option>
                                        Valencia
                                    </option>
                                </select-->
                        ";
        // line 150
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 150, $this->source); })()), "ciudad", [], "any", false, false, false, 150), 'label');
        echo "
                        ";
        // line 151
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 151, $this->source); })()), "ciudad", [], "any", false, false, false, 151), 'widget');
        echo "
                            </div>
                            <!--div class=\"infocliente__item\">
                                <label for=\"politica\">
                                    <input class=\"chulo\" type=\"checkbox\" name=\"privacidad\">
                                    He leído y acepto la política de privacidad
                                </label>
                            </-div>
                            <div class=\"infocliente__item\">
                                <input onclick=\"\" class=\"boton\" type=\"submit\" name=\"Enviar\">
                            </div>
                        </form-->
                        ";
        // line 163
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formulario"]) || array_key_exists("formulario", $context) ? $context["formulario"] : (function () { throw new RuntimeError('Variable "formulario" does not exist.', 163, $this->source); })()), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
        <section class=\"wrapper\">
            <h2 class=\"opinion__title heading-2\">
                Opiniones de otros guardianes
            </h2>
            <ul class=\"row\">
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
            </ul>
        </section>
        <section class=\"wrapper\">
            <h2 class=\"ubicacion__title heading-2\">
                Guardianes cerca tuyo
            </h2>
            <div class=\"row\">
                <div class=\"ubicacion__mapa col-xs-12 col-md-12\">
                    <iframe
                        src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d6075.046635414376!2d-3.691662212449509!3d40.41940965317808!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2ses!4v1575734913611!5m2!1ses-419!2ses\"
                        width=\"1000\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                </div>
            </div>
        </section>
    <footer class=\"footer\">
        <div class=\"wrapper\">
            <div class=\"despedida\">
                <img src=\"assets/logo.svg\" alt=\"logo maleteo\" class=\"despedida__logo\">
                <p class=\"despedida__amor\">
                    Hecho con ❤️en Madrid
                </p>
            </div>
        </div>
    </footer>
</body>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "maleteo.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  259 => 163,  244 => 151,  240 => 150,  221 => 134,  217 => 133,  205 => 124,  201 => 123,  189 => 114,  185 => 113,  174 => 105,  80 => 13,  70 => 12,  59 => 241,  57 => 12,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <link rel=\"stylesheet\" href=\"styles/styles.css\">
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran&display=swap\" rel=\"stylesheet\">
    <title>Maleteo</title>
</head>
{% block body%}
<body>
        <div class=\"logo\">
            <img src=\"assets/logo.svg\" alt=\"Logo Maleteo\">
        </div>
        <header class=\"compra\">
            <div class=\"wrapper\">
                <div class=\"container-fluid\">
                <section class=\"row\">
                    <div class=\"item col-xs-12 col-md-6\">
                        <h1 class=\"item__heading\">
                            Gana dinero guardando equipaje a viajeros como tu
                        </h1>
                        <ul class=\"app\">
                            <li class=\"app_tienda\">
                                <img src=\"assets/google-play.svg\" alt=\"Icono Google play\">
                            </li>
                            <li class=\"app_tienda\">
                                <img src=\"assets/app-store.svg\" alt=\"Icono\">
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
            </div>
        </header>
        <div class=\"wrapper\">
            <div class=\"how-works\">
                <h2 class=\"how-works__title heading-2\">
                    ¿Cómo funciona?
                </h2>
                <ol class=\"row\">
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                1
                            </p>
                        </div>
                        <div class=\"info\">
                            <p class=\"heading-3\">
                                Date de alta
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                2
                            </p>
                        </div>
                        <div>
                            <p class=\"heading-3\">
                                Publica tus espacios, horarios y tarifas
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                3
                            </p>
                        </div>
                        <div>
                            <p class=\"heading-3\">
                                Recibe viajeros y gana dinero
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                </ol>
            </div>
        </div>
        <div class=\"wrapper\">
            <div class=\"row\">
                <div class=\"movil col-xs-12 col-md-6\">
                    <img src=\"assets/iPhoneX.png\" alt=\"Foto iphone aplicación\" class=\"movil__item\">
                </div>
                <div class=\"col-xs-12 col-md-5\">
                    <div class=\"infocliente\">
                        <p class=\"heading-3\">
                            Solicita una demo
                        </p>
                        {{ form_start(formulario)}}
                            <div class=\"infocliente__item\">
                                <!--label for=\"nombre\">
                                    <p>
                                        Nombre
                                    </p>
                                </!--label>
                                <input type=\"text\" name=\"nombre\" placeholder=\" Vincent Chase\"-->
                        {{form_label(formulario.nombre)}}
                        {{form_widget(formulario.nombre) }}
                            </div>
                            <div class=\"infocliente__item\">
                                <!--label for=\"mail\">
                                    <p>
                                        Email
                                    </p>
                                </--label>
                                <input type=\"email\" name=\"mail\" placeholder=\"vincent@mga.com\"-->
                        {{form_label(formulario.mail) }}
                        {{form_widget(formulario.mail)}}
                            </div>
                            <div class=\"infocliente__item\">
                                <!--label for=\"horario\">
                                    <p>
                                        Horario preferido
                                    </p>
                                </!--label>
                                <input type=\"text\" name=\"horario\" placeholder=\"11:00h-13:00h\"-->
                        {{form_label(formulario.horario)}}
                        {{form_widget(formulario.horario)}}
                            </div>
                            <div class=\"infocliente__item\">
                                <!--label for=\"cuidad\">
                                    <p>
                                        Ciudad
                                    </p>
                                </!--label>
                                <select name=\"ciudades\">
                                    <option>
                                        Madrid
                                    </option>
                                    <option>
                                        Valencia
                                    </option>
                                </select-->
                        {{form_label(formulario.ciudad)}}
                        {{form_widget(formulario.ciudad)}}
                            </div>
                            <!--div class=\"infocliente__item\">
                                <label for=\"politica\">
                                    <input class=\"chulo\" type=\"checkbox\" name=\"privacidad\">
                                    He leído y acepto la política de privacidad
                                </label>
                            </-div>
                            <div class=\"infocliente__item\">
                                <input onclick=\"\" class=\"boton\" type=\"submit\" name=\"Enviar\">
                            </div>
                        </form-->
                        {{ form_end(formulario)}}
                    </div>
                </div>
            </div>
        </div>
        <section class=\"wrapper\">
            <h2 class=\"opinion__title heading-2\">
                Opiniones de otros guardianes
            </h2>
            <ul class=\"row\">
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
            </ul>
        </section>
        <section class=\"wrapper\">
            <h2 class=\"ubicacion__title heading-2\">
                Guardianes cerca tuyo
            </h2>
            <div class=\"row\">
                <div class=\"ubicacion__mapa col-xs-12 col-md-12\">
                    <iframe
                        src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d6075.046635414376!2d-3.691662212449509!3d40.41940965317808!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2ses!4v1575734913611!5m2!1ses-419!2ses\"
                        width=\"1000\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                </div>
            </div>
        </section>
    <footer class=\"footer\">
        <div class=\"wrapper\">
            <div class=\"despedida\">
                <img src=\"assets/logo.svg\" alt=\"logo maleteo\" class=\"despedida__logo\">
                <p class=\"despedida__amor\">
                    Hecho con ❤️en Madrid
                </p>
            </div>
        </div>
    </footer>
</body>
{% endblock body %}

</html>", "maleteo.html.twig", "/application/templates/maleteo.html.twig");
    }
}
