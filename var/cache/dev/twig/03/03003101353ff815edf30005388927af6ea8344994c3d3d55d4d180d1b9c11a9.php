<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* maleteo.html.twig */
class __TwigTemplate_54516666145e084af0c1998023917638115e2eb018d5a0c3a6468ea79f70251a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <link rel=\"stylesheet\" href=\\styles\\styles.css\">
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran&display=swap\" rel=\"stylesheet\">
    <title>Maleteo</title>
</head>

<body>
        <div class=\"logo\">
            <img src=\"images/maleteo/logo.svg\" alt=\"Logo Maleteo\">
        </div>
        <header class=\"compra\">
            <div class=\"wrapper\">
                <div class=\"container-fluid\">
                <section class=\"row\">
                    <div class=\"item col-xs-12 col-md-6\">
                        <h1 class=\"item__heading\">
                            Gana dinero guardando equipaje a viajeros como tu
                        </h1>
                        <ul class=\"app\">
                            <li class=\"app_tienda\">
                                <img src=\"public/images/maleteo/google-play.svg\" alt=\"Icono Google play\">
                            </li>
                            <li class=\"app_tienda\">
                                <img src=\"public/images/maleteo/app-store.svg\" alt=\"Icono\">
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
            </div>
        </header>
        <div class=\"wrapper\">
            <div class=\"how-works\">
                <h2 class=\"how-works__title heading-2\">
                    ¿Cómo funciona?
                </h2>
                <ol class=\"row\">
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                1
                            </p>
                        </div>
                        <div class=\"info\">
                            <p class=\"heading-3\">
                                Date de alta
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                2
                            </p>
                        </div>
                        <div>
                            <p class=\"heading-3\">
                                Publica tus espacios, horarios y tarifas
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                3
                            </p>
                        </div>
                        <div>
                            <p class=\"heading-3\">
                                Recibe viajeros y gana dinero
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                </ol>
            </div>
        </div>
        <div class=\"wrapper\">
            <div class=\"row\">
                <div class=\"movil col-xs-12 col-md-6\">
                    <img src=\"public/images/maleteo/iPhoneX.png\" alt=\"Foto iphone aplicación\" class=\"movil__item\">
                </div>
                <div class=\"col-xs-12 col-md-5\">
                    <div class=\"infocliente\">
                        <p class=\"heading-3\">
                            Solicita una demo
                        </p>
                        <form>
                            <div class=\"infocliente__item\">
                                <label for=\"nombre\">
                                    <p>
                                        Nombre
                                    </p>
                                </label>
                                <input type=\"text\" name=\"nombre\" placeholder=\" Vincent Chase\">
                            </div>
                            <div class=\"infocliente__item\">
                                <label for=\"mail\">
                                    <p>
                                        Email
                                    </p>
                                </label>
                                <input type=\"email\" name=\"mail\" placeholder=\"vincent@mga.com\">
                            </div>
                            <div class=\"infocliente__item\">
                                <label for=\"horario\">
                                    <p>
                                        Horario preferido
                                    </p>
                                </label>
                                <input type=\"text\" name=\"horario\" placeholder=\"11:00h-13:00h\">
                            </div>
                            <div class=\"infocliente__item\">
                                <label for=\"cuidad\">
                                    <p>
                                        Ciudad
                                    </p>
                                </label>
                                <select name=\"ciudades\">
                                    <option>
                                        Madrid
                                    </option>
                                    <option>
                                        Valencia
                                    </option>
                                </select>
                            </div>
                            <div class=\"infocliente__item\">

                                <label for=\"cuidad\">
                                    <input class=\"chulo\" type=\"checkbox\" name=\"privacidad\">
                                    He leído y acepto la política de privacidad
                                </label>
                            </div>
                            <div class=\"infocliente__item\">
                                <input onclick=\"\" class=\"boton\" type=\"submit\" name=\"Enviar\">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <section class=\"wrapper\">
            <h2 class=\"opinion__title heading-2\">
                Opiniones de otros guardianes
            </h2>
            <ul class=\"row\">
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
            </ul>
        </section>
        <section class=\"wrapper\">
            <h2 class=\"ubicacion__title heading-2\">
                Guardianes cerca tuyo
            </h2>
            <div class=\"row\">
                <div class=\"ubicacion__mapa col-xs-12 col-md-12\">
                    <iframe
                        src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d6075.046635414376!2d-3.691662212449509!3d40.41940965317808!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2ses!4v1575734913611!5m2!1ses-419!2ses\"
                        width=\"1000\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                </div>
            </div>
        </section>
    <footer class=\"footer\">
        <div class=\"wrapper\">
            <div class=\"despedida\">
                <img src=\"public/images/maleteo/logo.svg\" alt=\"logo maleteo\" class=\"despedida__logo\">
                <p class=\"despedida__amor\">
                    Hecho con ❤️en Madrid
                </p>
            </div>
        </div>
    </footer>
</body>

</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "maleteo.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <link rel=\"stylesheet\" href=\\styles\\styles.css\">
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran&display=swap\" rel=\"stylesheet\">
    <title>Maleteo</title>
</head>

<body>
        <div class=\"logo\">
            <img src=\"images/maleteo/logo.svg\" alt=\"Logo Maleteo\">
        </div>
        <header class=\"compra\">
            <div class=\"wrapper\">
                <div class=\"container-fluid\">
                <section class=\"row\">
                    <div class=\"item col-xs-12 col-md-6\">
                        <h1 class=\"item__heading\">
                            Gana dinero guardando equipaje a viajeros como tu
                        </h1>
                        <ul class=\"app\">
                            <li class=\"app_tienda\">
                                <img src=\"public/images/maleteo/google-play.svg\" alt=\"Icono Google play\">
                            </li>
                            <li class=\"app_tienda\">
                                <img src=\"public/images/maleteo/app-store.svg\" alt=\"Icono\">
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
            </div>
        </header>
        <div class=\"wrapper\">
            <div class=\"how-works\">
                <h2 class=\"how-works__title heading-2\">
                    ¿Cómo funciona?
                </h2>
                <ol class=\"row\">
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                1
                            </p>
                        </div>
                        <div class=\"info\">
                            <p class=\"heading-3\">
                                Date de alta
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                2
                            </p>
                        </div>
                        <div>
                            <p class=\"heading-3\">
                                Publica tus espacios, horarios y tarifas
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                    <li class=\"how-works__item col-xs-12 col-md-4\">
                        <div class=\"ball\">
                            <p class=\"letra\">
                                3
                            </p>
                        </div>
                        <div>
                            <p class=\"heading-3\">
                                Recibe viajeros y gana dinero
                            </p>
                            <p>
                                Una olla de algo más vaca que carnero, salpicón las más noches,
                                duelos y quebrantos los sábados.
                            </p>
                        </div>
                    </li>
                </ol>
            </div>
        </div>
        <div class=\"wrapper\">
            <div class=\"row\">
                <div class=\"movil col-xs-12 col-md-6\">
                    <img src=\"public/images/maleteo/iPhoneX.png\" alt=\"Foto iphone aplicación\" class=\"movil__item\">
                </div>
                <div class=\"col-xs-12 col-md-5\">
                    <div class=\"infocliente\">
                        <p class=\"heading-3\">
                            Solicita una demo
                        </p>
                        <form>
                            <div class=\"infocliente__item\">
                                <label for=\"nombre\">
                                    <p>
                                        Nombre
                                    </p>
                                </label>
                                <input type=\"text\" name=\"nombre\" placeholder=\" Vincent Chase\">
                            </div>
                            <div class=\"infocliente__item\">
                                <label for=\"mail\">
                                    <p>
                                        Email
                                    </p>
                                </label>
                                <input type=\"email\" name=\"mail\" placeholder=\"vincent@mga.com\">
                            </div>
                            <div class=\"infocliente__item\">
                                <label for=\"horario\">
                                    <p>
                                        Horario preferido
                                    </p>
                                </label>
                                <input type=\"text\" name=\"horario\" placeholder=\"11:00h-13:00h\">
                            </div>
                            <div class=\"infocliente__item\">
                                <label for=\"cuidad\">
                                    <p>
                                        Ciudad
                                    </p>
                                </label>
                                <select name=\"ciudades\">
                                    <option>
                                        Madrid
                                    </option>
                                    <option>
                                        Valencia
                                    </option>
                                </select>
                            </div>
                            <div class=\"infocliente__item\">

                                <label for=\"cuidad\">
                                    <input class=\"chulo\" type=\"checkbox\" name=\"privacidad\">
                                    He leído y acepto la política de privacidad
                                </label>
                            </div>
                            <div class=\"infocliente__item\">
                                <input onclick=\"\" class=\"boton\" type=\"submit\" name=\"Enviar\">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <section class=\"wrapper\">
            <h2 class=\"opinion__title heading-2\">
                Opiniones de otros guardianes
            </h2>
            <ul class=\"row\">
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
                <li class=\"opinion col-xs-12 col-md-4\">
                    <div class=\"opinion__cita\">
                        <blockquote>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam magnam placeat deleniti
                            sed adipisci dignissimos.
                        </blockquote>
                    </div>
                    <p class=\"opinion__guardian\">
                        Pepito Rodríguez
                    </p>
                    <p class=\"opinion__lugar\">
                        Tetuán, Madrid
                    </p>
                </li>
            </ul>
        </section>
        <section class=\"wrapper\">
            <h2 class=\"ubicacion__title heading-2\">
                Guardianes cerca tuyo
            </h2>
            <div class=\"row\">
                <div class=\"ubicacion__mapa col-xs-12 col-md-12\">
                    <iframe
                        src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d6075.046635414376!2d-3.691662212449509!3d40.41940965317808!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2ses!4v1575734913611!5m2!1ses-419!2ses\"
                        width=\"1000\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                </div>
            </div>
        </section>
    <footer class=\"footer\">
        <div class=\"wrapper\">
            <div class=\"despedida\">
                <img src=\"public/images/maleteo/logo.svg\" alt=\"logo maleteo\" class=\"despedida__logo\">
                <p class=\"despedida__amor\">
                    Hecho con ❤️en Madrid
                </p>
            </div>
        </div>
    </footer>
</body>

</html>", "maleteo.html.twig", "/application/templates/maleteo.html.twig");
    }
}
