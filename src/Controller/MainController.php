<?php

namespace App\Controller;

use App\Entity\Visitantes;
use App\Form\ArticuloFormType;
use App\Form\DemoFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
  
    /**
     * @Route("/", name="homepage")
     */
    public function maleteo(EntityManagerInterface $emi, Request $request)
    {
      $form = $this->createForm(DemoFormType::class);
      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid())
      {
        $visitantes = $form->getData();
        $emi->persist($visitantes);
        $emi->flush();

        return $this->redirectToRoute('homepage');
      }
      
        return $this->render(
          "maleteo.html.twig",
          [
            'formulario'=>$form->createView()
          ]
      
      );
    }
   
    
  //  /**
 //    * @Route("/articulo/new", name="nuevo-articulo")
   //  */
  //  public function nuevoArticulo(Request $request)
  //  {
 //       $articuloForm = $this->createForm(ArticuloFormType::class);
 //       $articuloForm->handleRequest($request);

  //      if ($articuloForm->isSubmitted() && $articuloForm->isValid()){
  //          dd($articuloForm->getData());
  //      }

  //      return $this->render('articulo/nuevo-articulo.html.twig',
  //      [
  //          'formulario'=> $articuloForm->createView()
   //     ]
   //     );
  //  }
}    
 
