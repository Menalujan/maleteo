<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Pelicula;

class PeliculaController extends AbstractController
{
    /**
     * @Route("/pelicula", name="pelicula")
     */
    public function index()
    {
        return $this->render('pelicula/index.html.twig', [
            'controller_name' => 'PeliculaController',
        ]);
    }
    /**
     * @Route("/pelicula/new", name="nueva-pelicula")
     */
    public function nuevaPelicula(EntityManagerInterface $em)
    {
        $pelicula = new Pelicula();
        $pelicula->setTitulo('Señor de los Anillos');
        $pelicula->setDescripcion('el anillo te hace invisible');

        $em->persist($pelicula);
        $em->flush();

        return $this->redirectToRoute('pelicula');
    }
}
