<?php

namespace App\Form;

use App\Entity\Visitantes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class DemoFormType extends AbstractType
{
 public function buildForm(FormBuilderInterface $builder, array $options)
 {
     $builder->add('nombre');
     $builder->add('mail', EmailType::class);
     $builder->add('horario');
     $builder->add('ciudad', ChoiceType::class,
     [
         'choices' =>
         [
            'Madrid' => 'Madrid',
            'Barcelona' => 'Barcelona',
            'Sevilla' => 'Sevilla'
         ],
         'placeholder' =>'Elige una opcion'
    ]);
    
    $builder->add ('privacidad', CheckboxType::class,
        [
            'label' => 'He leído y acepto la política de privacidad',
            'required' => true,
            'mapped' => false
        ]); 
    $builder->add('Enviar', SubmitType::class);
    }
    public function configureOptions(OptionsResolver $resolver) //Para manejar los datos del formulario de manera dinamica
     {
        $resolver->setDefaults(['data_class' => Visitantes::class]);
            
     }


}


