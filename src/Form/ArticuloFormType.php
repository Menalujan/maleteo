<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;


class ArticuloFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder->add('nombre');
       $builder->add
       (
           'descripcion',
           TextareaType::class,
           ['help' => 'Máximo 500 caracteres']
        );
       $builder->add('cantidad');
    }

}