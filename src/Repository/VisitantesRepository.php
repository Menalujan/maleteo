<?php

namespace App\Repository;

use App\Entity\Visitantes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Visitantes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Visitantes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Visitantes[]    findAll()
 * @method Visitantes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitantesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Visitantes::class);
    }

    // /**
    //  * @return Visitantes[] Returns an array of Visitantes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Visitantes
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
